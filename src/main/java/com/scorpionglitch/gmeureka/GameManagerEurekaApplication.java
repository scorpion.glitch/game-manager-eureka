package com.scorpionglitch.gmeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class GameManagerEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameManagerEurekaApplication.class, args);
	}

}
